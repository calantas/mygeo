## myGeo
<img src="https://mygeo.myridia.com/img/mygeo.png" width="90" height="90">

Share your Geo Location as Latitude and Longitude with your own template.

You can customize a template with the latitude and longitude and share it to other apps.

### Features
* Works in offline Mode
* Large accessible buttons to share quick your latitude and longitude
* Create your own share template with an editor
* Use net or satellite native app services what works on offline mode
* Quick to use when user is on the field 

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/org.calantas.mygeo/)

### How do I set it up? 

#### Download the sdk 
https://developer.android.com/studio/#command-tools look for a commandline tool like: sdk-tools-linux-4333796.zip

#### Unzip the downloaded file: 
```bash
unzip commandlinetools-linux-xxxxx_latest.zip
``` 

#### Create the folders android-sdk/platform-tools in /opt/
```bash
sudo mkdir -p /opt/android-sdk/cmdline-tools/
```

#### Move the folder cmdline-tools to the created folder and name it tools/ 
```bash
sudo cp cmdline-tools /opt/android-sdk/cmdline-tools/tools
```

#### Make it accessble for all users: 
```bash
sudo chmod 777 /opt/android-sdk -Rf
```

#### Your folder structure should look like like: 
```text
├── android-sdk
│   ├── cmdline-tools
│   │   └── tools
│   │       ├── bin
│   │       ├── lib
│   │       ├── NOTICE.txt
│   │       └── source.properties

```

#### Check where your java-11-openjdk is installed and eventual create syslink if the name is different
```bash
ln -s /usr/lib/jvm/java-11-openjdk-amd64 /usr/lib/jvm/java-11-openjdk
```


#### Edit your ~/.bashrc file like below to have the enviorment variable loaded, 
```bash
export JAVA_HOME='/usr/lib/jvm/java-11-openjdk'  
export ANDROID_HOME="/opt/android-sdk"  
export PATH=$PATH:$ANDROID_HOME/cmdline-tools  
export PATH=$PATH:$ANDROID_HOME/cmdline-tools/tools/bin  
export PATH=$PATH:$ANDROID_HOME/platform-tools/bin  
```

#### Reload your enviroment with: 
```bash
source ~/.bashrc 
```

#### Execute the below sdkmanager commands 
```bash
yes | sdkmanager --licenses
sdkmanager ndk-bundle
sdkmanager "build-tools;23.0.3"
sdkmanager "platform-tools" "platforms;android-30"
```

### Compile the debug APK 
```bash
./gradlew assembleDebug
```

### Compile an unsigned release APK
```bash
./gradlew assembleRelease
```

### Compile a debug APK and install the APK to your connected device
```bash
./gradlew installDebug 
```

### Screenshots 
#### Main Screen
![Main Screen](https://mygeo.myridia.com/img/Screenshot_20220607-104530_myGeo.png)

#### Edit Template Screen
![Edit Template Screen](https://mygeo.myridia.com/img/Screenshot_20220607-104555_myGeo.png)

#### Setting Screen
![Setting Screen](https://https://mygeo.myridia.com/img/Screenshot_20220607-104544_myGeo.png)

### License
GNU General Public License, version 3

![GNU General Public License, version 3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png "GNU General Public License, version 3")

