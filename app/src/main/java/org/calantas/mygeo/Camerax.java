/**
 * License: GPL-3.0-or-later
 * Author: veto@myridia.com
 * Description: Collection of reused methods for this app
 */
package org.calantas.mygeo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Color;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.app.ShareCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.google.common.util.concurrent.ListenableFuture;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Date;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


public class Camerax extends AppCompatActivity {
    private final Executor executor = Executors.newSingleThreadExecutor();
    Lib lib;
    Button capture, toggleFlash, flipCamera;
    PreviewView preview_view;







    public String get_data(String _key) {
        Context context = getApplicationContext();
        SharedPreferences pref = context.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        return pref.getString(_key, "");
    }

    private void startCamera() {
        final ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);
        cameraProviderFuture.addListener(new Runnable() {
            @Override
            public void run() {
                try {

                    ProcessCameraProvider camera_provider = cameraProviderFuture.get();
                    //logme(camera_provider.toString());
                    bindPreview(camera_provider);

                } catch (ExecutionException | InterruptedException e) {
                    // No errors need to be handled for this Future.
                    // This should never be reached.
                }
            }
        }, ContextCompat.getMainExecutor(this));
    }


    void bindPreview(@NonNull ProcessCameraProvider camera_provider) {
        Preview preview = new Preview.Builder().build();

        CameraSelector cameraSelector = new CameraSelector.Builder().requireLensFacing(CameraSelector.LENS_FACING_BACK).build();
        ImageAnalysis imageAnalysis = new ImageAnalysis.Builder().build();
        ImageCapture.Builder builder = new ImageCapture.Builder();
        final ImageCapture imageCapture = builder.setTargetRotation(this.getWindowManager().getDefaultDisplay().getRotation()).build();
        preview_view = findViewById(R.id.cameraView);
        final Preview.SurfaceProvider[] x = {preview_view.getSurfaceProvider()};
        preview.setSurfaceProvider(x[0]);
        Camera camera = camera_provider.bindToLifecycle(this, cameraSelector, preview, imageAnalysis, imageCapture);


        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText msg = findViewById(R.id.msg);
                msg.setText("");


                logme("...click shutter");
                SimpleDateFormat date_format = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
                SimpleDateFormat date_format2 = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.US);

                long t = System.currentTimeMillis();

                Date c = new Date(t);
                String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString() + "/myGeo";
                if(! Files.exists(Paths.get(path))) {
                    File directory = new File(path);
                    boolean dr = directory.mkdir();
                }
                String file_name = date_format.format(c) + ".jpg";
                File file = new File(path, file_name);
                ImageCapture.OutputFileOptions.Builder shot = new ImageCapture.OutputFileOptions.Builder(file);


                ImageCapture.OutputFileOptions out = shot.build();

                imageCapture.takePicture(out, executor, new ImageCapture.OnImageSavedCallback() {
                    @Override
                    public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                        //logme("...saved picture");
                        Intent i = new Intent(Intent.ACTION_SEND);


                        String last_lat = get_data("lat");
                        String last_long = get_data("lon");
                        String last_date = lib.get_saved_date_data(getApplicationContext());
                        Context context = getApplicationContext();
                        SharedPreferences pref = context.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                        String draft = pref.getString("template", "");
                        String subject = MessageFormat.format("{0},{1}", last_lat, last_long);
                        String body = MessageFormat.format(draft, last_lat, last_long, last_date);

                        i.setType("image/*");

                        i.putExtra(Intent.EXTRA_SUBJECT, subject);
                        i.putExtra(Intent.EXTRA_TEXT, body);
                        //storage/emulated/0/Pictures/myGeo/20240626191421.jpg


                        //Object BuildConfig;
                        Context c = getApplicationContext();
                        int rotation = 0;
                        try {
                            ExifInterface exif = new ExifInterface(file);
                            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

                            if      (orientation == 6)      rotation = 90;
                            else if (orientation == 3)      rotation = 180;
                            else if (orientation == 8)      rotation = 270;
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                        Matrix matrix = new Matrix();
                        matrix.postRotate(rotation);

                        // Bitmap bmp =  Bitmap.createBitmap(500, 500 , Bitmap.Config.ARGB_8888);
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inMutable = true;
                        options.inDensity = 0;

                        Bitmap _bmp = BitmapFactory.decodeFile(file.getPath(),options);
                        int height = _bmp.getHeight();
                        int width = _bmp.getWidth();
                        String orientation = "landscape";
                        if(height > width ) {
                            orientation = "portrait";
                        }
                        Bitmap bmp = Bitmap.createBitmap(_bmp, 0, 0, width, height, matrix, true);


                        //logme("width: " + width);
                        //logme("height: " + height);



                       // logme("orientation: " + orientation);
                        //logme("rotation: " + rotation);

                        Canvas canvas = new Canvas(bmp);

                        Paint paint = new Paint();
                        paint.setColor(Color.WHITE);
                        paint.setTextSize(150);
                        logme(String.valueOf(canvas.getHeight()));
                        logme(String.valueOf(canvas.getWidth()));
                        //double _lat = 13.74529259;
                        //double _long = 100.32648065;


                        String _cord = "Lat/Long: " + last_lat + "," + last_long;

                        long _t = System.currentTimeMillis();

                        Date _c = new Date(_t);
                        SimpleDateFormat date_format2 = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.US);



                        String _date = "Date: " + date_format2.format(_c);
                        canvas.drawText(_date, 50,canvas.getHeight()-250, paint);
                        canvas.drawText(_cord, 50,canvas.getHeight()-50, paint);

                        //File filename = new File(path, "test.jpg");

                        try (FileOutputStream out2 = new FileOutputStream(file)) {
                            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out2); // bmp is your Bitmap instance
                            // PNG is a lossless format, the compression factor (100) is ignored


                          //  String _data = "Hello world";


                            if(last_long !="" && last_lat != "" ) {
                                ExifInterface exif = new ExifInterface(file);
                                exif.setAttribute("ImageDescription", _date + "|" + _cord);

                            exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, lib.dec2DMS(Double.parseDouble(last_long)));
                            exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, lib.dec2DMS(Double.parseDouble(last_lat)));
                            //logme("xxxxxxxxxxxxxxxxxxxxxxxxxxxx " +  _cord);
                            if (Double.parseDouble(last_lat) > 0)
                                exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "N");
                            else
                                exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "S");
                            if (Double.parseDouble(last_long) >0)
                                exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "E");
                            else
                                exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "W");

                            exif.saveAttributes();
}



                        } catch (IOException e) {
                            e.printStackTrace();
                        }



                        Uri uri = FileProvider.getUriForFile(c, "org.calantas.mygeo", file);

                        //Uri uri = FileProvider.getUriForFile(c, getPackageName(), file);
                        List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(i, PackageManager.MATCH_DEFAULT_ONLY);

                        for (ResolveInfo resolveInfo : resInfoList) {
                            String packageName = resolveInfo.activityInfo.packageName;
                            context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        }






                        //Uri uri = Uri.parse(file.getPath());
                        i.putExtra(Intent.EXTRA_STREAM, uri);
                        startActivity(Intent.createChooser(i, "Share Image"));




                    }

                    @Override
                    public void onError(@NonNull ImageCaptureException error) {
                        error.printStackTrace();
                        logme("...error");
                    }
                });

            }


        });

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        lib = new Lib(getApplicationContext());
        setContentView(R.layout.camerax);
        Toolbar toolbar = findViewById(R.id.my_toolbar); //find the toolbar from the xml layout
        toolbar.setTitle("myGeo - Main");
        setSupportActionBar(toolbar); // add toolbar to the main screen
        boolean p = lib.allPermissionsGranted();
        if (p) {

            startCamera();
        }

        capture = findViewById(R.id.capture);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return lib.drop_menu(item, this, findViewById(R.id.msg));
    }

    private void logme(String message) {
        lib.logme(message, findViewById(R.id.msg), true);
    }


}
