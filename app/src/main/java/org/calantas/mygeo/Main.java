/**
 * License: GPL-3.0-or-later
 * Author: veto@myridia.com
 * Description: Main Activity Screen
 */
package org.calantas.mygeo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.GnssStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.text.MessageFormat;
import java.util.Timer;
import java.util.TimerTask;

public class Main extends AppCompatActivity {
    LocationManager lm;
    int logcounter = 1;
    boolean app_ok = false;

    boolean gps;
    boolean net;
    int satellites = 0;
    Lib lib;

    MediaPlayer amp;
    LocationListener listen_callback = new LocationListener() {
        @Override
        public void onLocationChanged(android.location.Location loc) {
            //logme("...location changed");
            Double last_lat = loc.getLatitude();
            Double last_long = loc.getLongitude();
            set_status(last_lat, last_long);
            stop_graphic_status_animation();
            save_cord_data(String.valueOf(last_lat), String.valueOf(last_long));
            lib.save_date_data(getApplicationContext());
        }
    };
    private Gnss_callback gps_callback;


    private void logme(String message) {
        logcounter += 1;
        lib.logme(logcounter + ". " + message, findViewById(R.id.msg), true);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);     // set the main window
        lib = new Lib(getApplicationContext()); //init general used function class Lib.java
        //logme("...successfully created main activity windows ");
        Toolbar toolbar = findViewById(R.id.my_toolbar); //find the toolbar from the xml layout
        toolbar.setTitle("myGeo - Main");
        setSupportActionBar(toolbar); // add toobar to the main screen

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logme("...click logo icon");
            }
        });

        //timer_debug();

        //request_permission();

        boolean p = lib.allPermissionsGranted();
        if (p) {
            logme("...permission ok, activate mygeo");
            start_geo_location(); // init the geo location
            start_graphic_status_animation(); // animate the status icons
            share_click_listen(); // listen on graphic clicks
        } else {

            lib.request_permission(this);
        }


    }

    @Override
    public void onRequestPermissionsResult(int request_code, String[] permissions, int[] grant_results) {
        //logme("...request code " + String.valueOf(request_code));
        boolean p = lib.allPermissionsGranted();
        if (p && !app_ok) {
            logme("...permission ok, activate mygeo");
            app_ok = true;
            start_geo_location(); // init the geo location
            start_graphic_status_animation(); // animate the status icons
            share_click_listen(); // listen on graphic clicks

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //logme("...start main activity");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return lib.drop_menu(item, this, findViewById(R.id.msg));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //logme("...restart Activity");
    }

    @Override
    protected void onResume() {
        super.onResume();
        //logme("...resume Activity");
    }

    @Override
    protected void onPause() {
        super.onPause();
        //logme("...pause Activity");
    }

    @Override
    protected void onStop() {
        super.onStop();
        //logme("...stop Activity");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void start_geo_location() {
        //logme("...start geo location");

        lm = (LocationManager) getSystemService(LOCATION_SERVICE); //init location manager
        gps = lm.isProviderEnabled(LocationManager.GPS_PROVIDER); // check if can use GPS to get the cords
        net = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER); // check if can use Net to get the cords

        if (net) {
            //logme("...net service is activated, use it");
            lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, listen_callback);
        } else if (gps) {
            //logme("...gps service is activated, use it");
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, listen_callback);
            gps_callback = new Gnss_callback(this.satellites, lm);
            lm.registerGnssStatusCallback(gps_callback);
        } else {
            //logme("...none Geo Service is activated, cannot use this app");
        }

    }

    public void alert() {
        Boolean alert = lib.get_alert(getApplicationContext());
        if (alert && amp == null) {
            Uri sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.sos);
            amp = MediaPlayer.create(getApplicationContext(), sound);
            amp.setVolume(100, 100);
            amp.start();
        }
    }


    public String get_data(String _key) {
        Context context = getApplicationContext();
        SharedPreferences pref = context.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        return pref.getString(_key, "");
    }

    public void save_cord_data(String _lat, String _lon) {
        Context context = getApplicationContext();
        SharedPreferences pref = context.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("lat", _lat);
        editor.putString("lon", _lon);
        editor.commit();
    }

    public void set_status(Double _lat, Double _lon) {
        TextView l = findViewById(R.id.last_fix);
        l.setText("Last Fix: " + _lat + "," + _lon);
        //  logme("...latitude: " + String.valueOf(_lat));
        //  logme("...longitude: " + String.valueOf(_lon));
    }

    public void start_graphic_status_animation() {
        ImageView image = findViewById(R.id.earth);
        Animation ani = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);
        image.startAnimation(ani);

        ImageView image1 = findViewById(R.id.sat1);
        Animation ani1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink);
        image1.startAnimation(ani1);

        ImageView image2 = findViewById(R.id.sat2);
        Animation ani2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink);
        image2.startAnimation(ani2);
    }

    public void stop_graphic_status_animation() {
        ImageView image = findViewById(R.id.earth);
        Animation ani = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);
        image.clearAnimation();

        ImageView image1 = findViewById(R.id.sat1);
        Animation ani1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink);
        image1.clearAnimation();

        ImageView image2 = findViewById(R.id.sat2);
        Animation ani2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink);
        image2.clearAnimation();
    }

    public void share_click_listen() {
        final Button btn_share_template = findViewById(R.id.btn_share_template);
        btn_share_template.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //logme("...click share template");
                String last_lat = get_data("lat");
                String last_long = get_data("lon");
                String last_date = lib.get_saved_date_data(getApplicationContext());
                String subject = MessageFormat.format("{0},{1}", last_lat, last_long);
                String body = MessageFormat.format(lib.get_template(), last_lat, last_long, last_date);
                Intent intentt = new Intent(Intent.ACTION_SEND);
                intentt.setType("text/plain");
                intentt.putExtra(Intent.EXTRA_SUBJECT, subject);
                intentt.putExtra(Intent.EXTRA_TEXT, body);
                startActivity(Intent.createChooser(intentt, "Share Via"));
                //logme("...latitude: " + last_lat);
                //logme("...longitude: " + last_long);
            }

        });

         /*
        final Button btn_share_my_template = findViewById(R.id.btn_share_my_template);
        btn_share_my_template.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //logme("...click share my template");
                String last_lat = get_data("lat");
                String last_long = get_data("lon");
                String last_date = lib.get_saved_date_data(getApplicationContext());
                Context context = getApplicationContext();
                SharedPreferences pref = context.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                String draft = pref.getString("template", "");
                String subject = MessageFormat.format("{0},{1}", last_lat, last_long);
                String body = MessageFormat.format(draft, last_lat, last_long, last_date);

                Intent intentt = new Intent(Intent.ACTION_SEND);
                intentt.setType("text/plain");
                intentt.putExtra(Intent.EXTRA_SUBJECT, subject);
                intentt.putExtra(Intent.EXTRA_TEXT, body);
                startActivity(Intent.createChooser(intentt, "Share Via"));
            }
        });

        final Button btn_share_cords = findViewById(R.id.btn_share_cords);
        btn_share_cords.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //logme("...click share cords");
                String last_lat = get_data("lat");
                String last_long = get_data("lon");
                String last_date = lib.get_saved_date_data(getApplicationContext());
                String subject = MessageFormat.format("geo:{0},{1}", last_lat, last_long);
                String body = MessageFormat.format("geo:{0},{1} \n\ndate: {2}", last_lat, last_long, last_date);

                Intent intentt = new Intent(Intent.ACTION_SEND);
                intentt.setType("text/plain");
                intentt.putExtra(Intent.EXTRA_SUBJECT, subject);
                intentt.putExtra(Intent.EXTRA_TEXT, body);
                startActivity(Intent.createChooser(intentt, "Share Via"));
            }
        });
        */

        final Button btn_share_camera = findViewById(R.id.btn_share_camera);
        btn_share_camera.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //logme("...click share camera");
                Context context = getApplicationContext();
                Intent cam = new Intent(context, Camerax.class);
                cam.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(cam);
            }
        });


    }

    public void timer_debug() {

        Timer t = new Timer();
        TimerTask timer_task = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //logme("...timer");
                        //alert();
                        //lib.save_date_data(getApplicationContext());
                        //String last_date = lib.get_saved_date_data(getApplicationContext());
                        //logme(last_date);
                    }
                });
            }
        };
        t.schedule(timer_task, 5000, 5000);
    }

    private class Gnss_callback extends GnssStatus.Callback {
        int satellites;
        TextView s = findViewById(R.id.sat_count);

        Gnss_callback(int sat, LocationManager _lm) {
            satellites = sat;
            lm = _lm;
        }

        @Override
        public void onFirstFix(int ttffMillis) {
            //logme("...got first fix");
            LocationManager locm = (LocationManager) getSystemService(LOCATION_SERVICE); //init location manager
            Location ll = locm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Double last_lat = ll.getLatitude();
            Double last_long = ll.getLongitude();
            set_status(last_lat, last_long);
            stop_graphic_status_animation();
            save_cord_data(String.valueOf(last_lat), String.valueOf(last_long));
            lib.save_date_data(getApplicationContext());
            alert();
        }

        @Override
        public void onSatelliteStatusChanged(GnssStatus status) {
            if (satellites != status.getSatelliteCount()) {
                satellites = status.getSatelliteCount();
                s.setText("Satellites: " + status.getSatelliteCount());
                //logme("...satellite Counts " + status.getSatelliteCount());
            }
        }
    }


}


