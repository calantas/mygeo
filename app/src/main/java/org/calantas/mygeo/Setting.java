/**
 * License: GPL-3.0-or-later
 * Author: veto@myridia.com
 * Description: Setting Acitivy Screen
 */
package org.calantas.mygeo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;

public class Setting extends PreferenceActivity {
    Lib lib;
    private AppCompatDelegate mDelegate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getDelegate().installViewFactory();
        getDelegate().onCreate(savedInstanceState);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        lib = new Lib(getApplicationContext());
        logme("...on create");

        addPreferencesFromResource(R.layout.preferences);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("myGeo - Settings");
        toolbar.inflateMenu(R.menu.menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                logme("... option item select");
                Context context = getApplicationContext();
                return lib.drop_menu(item, context, findViewById(R.id.msg));
            }
        });


        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logme("...click logo icon");
                Intent intent = new Intent(getApplicationContext(), Main.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onPostCreate(Bundle instance) {
        super.onPostCreate(instance);
        getDelegate().onPostCreate(instance);
        logme("...on post create");
    }

    @Override
    public void setContentView(@LayoutRes int layout_id) {
        getDelegate().setContentView(layout_id);
        //logme("...set content view");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        getDelegate().onPostResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        getDelegate().onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getDelegate().onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        logme("... option item select");
        return lib.drop_menu(item, this, findViewById(R.id.msg));
    }


    private void setSupportActionBar(@Nullable Toolbar toolbar) {
        //getDelegate().setSupportActionBar(toolbar);
    }

    private AppCompatDelegate getDelegate() {
        if (mDelegate == null) {
            mDelegate = AppCompatDelegate.create(this, null);
        }
        return mDelegate;
    }

    private void logme(String message) {
        lib.logme(message, findViewById(R.id.logger_setting), false);
    }


}
